package edu.umich.icpsr.c2metadata.controller;


import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.umich.icpsr.c2metadata.service.CodeBookService;
import edu.umich.icpsr.c2metadata.util.CodebookBuilder;

@Controller
@RequestMapping("/codebook")
public class CodeBookController {

	private static final Logger LOG = Logger.getLogger(CodeBookController.class);

	@Autowired
	private CodeBookService codeBookService;
		
	@Autowired
	@Qualifier("codebookBuilder")
	private CodebookBuilder codebookBuilder;

	@RequestMapping(value="/text", method = RequestMethod.POST)
	public String generateCodeBookAsText(@RequestParam MultipartFile ddiFile,
			HttpServletRequest req, 
			HttpServletResponse resp) {
		LOG.info("Executing parseSdtl.");
		
		try {
		String filePath =  "/usr/local/c2metadata/ddi/" + UUID.randomUUID().toString();		
		new File(filePath).mkdirs();
		filePath += "/ddi.xml";
		ddiFile.transferTo(new File(filePath));
		 return codeBookService.generateCodebook(filePath,false);
		}catch(Exception e) {
			LOG.error(e.getMessage(),e);
		}
		return null;
	}		
	@RequestMapping(value = "/html", method = RequestMethod.POST)
	public void generateCodebookAsFile(
			@RequestParam MultipartFile ddiFile,
			HttpServletRequest req,
			HttpServletResponse resp) {
		LOG.info("Executing generateCodebook.");
		try {
			String filePath =  "/usr/local/c2metadata/ddi/" + UUID.randomUUID().toString();		
			new File(filePath).mkdirs();
			filePath += "/ddi.xml";
			ddiFile.transferTo(new File(filePath));
			String html=codeBookService.generateCodebook(filePath,false);
			if(html!=null && !html.isEmpty()) {
				convertStringToDownload("codebook.html", html, resp);
			} 
		} catch(Exception e){
			LOG.error(e);
		}
		
	}
	@ResponseBody private static void convertStringToDownload(
			String filename,
			String fileContent,
			HttpServletResponse response
			){
		try {
			OutputStream out = response.getOutputStream();
			response.setContentType("text/plain; charset=utf-8");
			response.addHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");
			out.write(fileContent.getBytes(Charset.forName("UTF-8")));
			out.flush();
			out.close();
		  } catch (IOException e) {
			  LOG.error("Error while trying to trigger citation export.");
			  LOG.error(e);
		  }		
	}
	
}
